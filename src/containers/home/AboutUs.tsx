import { Box, Flex, Heading, Text,Button, Image } from "@chakra-ui/react";
import React from "react";

type Props = {};

const AboutUs: React.FC<{}> = (props: Props) => {
    return (
        <>
            <Flex bg="bg.blueishGray" mt={{ xs: "64px", md: "150px" }} mb="100px">
                <Box display={{xs:"none",md:"block"}} flex={0.52} pl={{md:"52"}}>
					<Image h={"100%"} src="/assets/aboutUs.png" alt="AboutUs" />
				</Box>
				<Box
                    flex={{xs:1,md:0.48}}
                    w="full"
                    textAlign={{ xs: "center",md:"left" }}
                    py={{ xs: "8.75rem" }}
                >
                    <Heading
                        fontFamily="poppins"
                        fontWeight="semibold"
                        fontSize={{ xs: "1.625rem", md: "2.8125rem" }}
                        lineHeight={{ xs: "2.1875rem", md: "1.625rem" }}
                        color="primaryBlue"
                    >
                        About Us
                    </Heading>
                    <Text
                        mt={{ xs: "1.3125rem", md: "1.5rem" }}
						noOfLines={6}
						color="text.secondaryBlue"
						fontFamily="open"
						fontWeight="400"
						fontSize={{xs:"0.6875rem",md:"0.9375rem"}}
						lineHeight={{xs:"1.3125rem",md:"1.6875rem"}}
						// px={{xs:"50px",md:0}}
						w={{xs:"44ch",md:"53ch"}}
						mx={{xs:"auto",md:0}}
                    >
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry&apos;s standard dummy text ever since the
                        1500s, when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. t has
                        survived not only five centuries.
                    </Text>
                    {/* <Button
                        border="0.125rem"
                        borderColor="white"
                        bg="primaryRed"
                        textColor="white"
                        fontFamily="source"
                        fontSize={{ xs: "0.7669rem", md: "1.125rem" }}
                        letterSpacing="0.01em"
                        fontWeight="semibold"
                        // py={{ xs: "4px", md: "16px" }}
                        px={{ xs: "60px", md: "34px" }}
                        borderRadius="full"
						size={{xs:"sm",md:"md"}}
                        mx={{ xs: "auto", md: "0" }}
                        mt={{ xs: "1.3494rem", md: "21.59px" }}
                    >
                        Read More
                    </Button> */}
                    <Button
                        fontFamily="source"
                        fontSize={{ xs: "0.7669rem", md: "1.125rem" }}
                        fontWeight="semibold"
						size={{xs:"sm",md:"lg"}}
						variant="filled"
                        mx={{ xs: "auto", md: "0" }}
                        mt={{ xs: "1.3494rem", md: "21.59px" }}
                    >
                        Read More
                    </Button>
                </Box>
            </Flex>
        </>
    );
};

export default AboutUs;
