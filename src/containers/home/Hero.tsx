import {
    Box,
    Button,
    Flex,
    Heading,
    Highlight,
    Text,
    Image,
} from "@chakra-ui/react";
import React from "react";

type Props = {};

const Hero: React.FC<{}> = () => {
    return (
        <>
            <Flex alignItems={{md:"flex-start"}} flexDirection={{ xs: "column-reverse", md: "row" }}>
                <Box mt={{md:'227px'}} borderColor="blue" pl={{md:"100px"}} flex={1} textAlign={{xs:"center",md:"left"}} pt={{xs:"63px",md:"0"}}>
                    <Heading
                        w={{ xs: "13ch", md: "11ch" }}
                        noOfLines={3}
                        lineHeight={{ xs: "2.875rem", md: "4.1875rem" }}
                        fontSize={{ xs: "2.375rem", md: "3.875rem" }}
                        fontFamily="source"
                        fontWeight="bold"
                        color="primaryBlue"
                        textAlign={{ xs: "center", md: "left" }}
                        mx={{ xs: "auto", md: "0" }}
                    >
                        <Highlight
                            query="Best"
                            styles={{ color: "primaryRed" }}
                        >
                            Discover the Best Food and Drinks
                        </Highlight>
                    </Heading>
                    <Text
                        fontFamily="open"
                        lineHeight={{ xs: "1.125rem", md: "1.7131rem" }}
                        fontSize={{ xs: "0.6875rem", md: "1.0275rem" }}
                        w="40ch"
                        mx={{ xs: "auto", md: "0" }}
                        mt={{ xs: "1.3125rem", md: "1.375rem" }}
                    >
                        Naturally made Healthcare Products for the better care &
                        support of you body.
                    </Text>
                    <Button
					variant="filled"
					size={{xs:"md",md:"mdDesktop"}}
					fontFamily="open"
					fontWeight={"700"}
                    fontSize={{ xs: "12.2664px", md: "1.125rem" }}
					mx={{ xs: "auto", md: "0" }}
                    mt={{ xs: "1.75rem", md: "2.375rem" }}
					>
                        Explore Now!
                    </Button>
                    {/* <Button
                        border="0.125rem"
                        borderColor="white"
                        bg="primaryRed"
                        textColor="white"
                        fontFamily="source"
                        fontSize={{ xs: "0.7669rem", md: "1.125rem" }}
                        letterSpacing="0.01em"
                        fontWeight="semibold"
                        lineHeight={{ xs: "32px", md: "36px" }}
                        py={{ xs: "4px", md: "0" }}
                        px={{ xs: "18px", md: "34px" }}
                        borderRadius="full"
						size={{xs:"lg",md:"lg"}}
                        mx={{ xs: "auto", md: "0" }}
                        mt={{ xs: "1.75rem", md: "2.375rem" }}
                    >
                        Explore Now!
                    </Button> */}
                </Box>
                <Box flex={1} position="relative">
                    <Image
                        display={{ xs: "block", md: "none" }}
                        position={"absolute"}
                        src="/assets/HeroOverlay.png"
                        alt="HeroOverlay"
                    />
                    <Image
                        display={{ xs: "block", md: "none" }}
                        alt="Hero"
                        src="/assets/hero.png"
                    />
                    <Image
                        display={{ xs: "none", md: "block" }}
                        alt="Hero"
						w="full"
                        src="/assets/heroCombined.png"
                    />
                </Box>
            </Flex>
        </>
    );
};

export default Hero;
