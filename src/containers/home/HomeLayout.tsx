import Navbar from "@/components/Navbar";
import React from "react";
import AboutUs from "./AboutUs";
import Hero from "./Hero";

type Props = {};

const HomeLayout: React.FC<{}> = () => {
    return (
        <>
            <div className="page-wrapper" style={{position:"relative",width:"100%",minHeight:"100vh" }}>
				<Navbar/>
				<Hero/>
				<AboutUs/>
			</div>
        </>
    );
};

export default HomeLayout;
