export const colors = {
	primaryRed:"#E23744",
	primaryBlue:"#0E2368",
	bg:{
		lightGray:"#f8f8f8",
		blueishGray:"#f0f1f7"
	},
	text:{
		secondaryBlue:"#444957"
	},
	gray:{
		light:"#f8f8f8"	
	}
}