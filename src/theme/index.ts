import {extendTheme } from "@chakra-ui/react";
import { colors } from "./colors";
import { breakpoints } from "./breakpoints";

import { Button } from "./components/button";


const theme = extendTheme({
    colors,
    breakpoints,
    fonts: {
        open: "Open Sans,sans-serif",
        poppins: "Poppins,sans-serif",
        source: "Source Sans Pro,sans-serif",
    },
	components:{
		Button
	}
});

export default theme;
