import Head from "next/head";
import React from "react";

type Props = {};

const Layout: React.FC<{
    children: React.ReactNode;
    title?: string;
}> = ({ children, title }) => {
    return (
        <>
            <Head>{title && <title>{title}</title>}</Head>
            {children}
        </>
    );
};

export default Layout
