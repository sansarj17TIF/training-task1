import { Box, Button, Flex,Img } from "@chakra-ui/react";
import React from "react";

type Props = {};

const Navbar: React.FC<{}> = () => {
    return (
        <>
            <Flex
                w="full"
                bg="transparent"
                pos={"absolute"}
                zIndex="10"
                h={"100px"}
                alignItems="flex-start"
                justify={{ xs: "flex-end", md: "space-between" }}
                pt={{ xs: "18px", md: "32px" }}
                pr={{ xs: "18px", md: "40px" }}
                pl={{ xs: "0", md: "100px" }}
            >
                <Box
                    overflow={"visible"}
                    w="108px"
                    display={{ xs: "none", md: "block" }}
                >
                    <Img
                        style={{ width: "full" }}
                        src={"/assets/MainLogo.png"}
                        alt="MainLogo"
                    />
                </Box>
                {/* <Button
                    bg="transparent"
                    textColor="white"
                    fontFamily="source"
                    fontSize={{ xs: "11px", md: "16px" }}
                    fontWeight="medium"
					letterSpacing={"0.03em"}
					// lineHeight={{xs:"46px"}}
                    borderColor="white"
                    borderRadius="full"
                    borderWidth={"1px"}
					size={{xs:"sm",md:"lg"}}
                    // py={{xs:"3.5",md:"5"}}
                    px={{xs:"3",md:"4"}}
                >
                    Get In Touch
                </Button> */}
                <Button
                    fontFamily="source"
                    fontSize={{ xs: "11px", md: "16px" }}
                    fontWeight="600"
					letterSpacing="0.03em"
                    size={{ xs: "sm", md: "smDesktop" }}
                    variant="transparent"
                >
                    Get In Touch
                </Button>
            </Flex>
        </>
    );
};

export default Navbar;
