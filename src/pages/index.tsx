import { Inter } from "next/font/google";
import Layout from "@/components/Layout";
import HomeLayout from "@/containers/home/HomeLayout";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Layout title="Home Page">
        <HomeLayout />
      </Layout>
    </>
  );
}
